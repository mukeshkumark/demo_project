# Occupation Automation Index Changelog

All notable changes to this project will be documented in this file.

If you have questions, please contact us by emailing our engineering team at [apisupport@emsibg.com](mailto:apisupport@emsibg.com).

## July 2020

### 1.1.2 (July 13)

#### Changed
* US data updated, using 2019 SOC codes.

## March 2020

### 1.1.1 (March 11)

#### Refactored
* Changed API tech stack.

#### Removed
* `/meta` and `/data` deprecated endpoints.

## October 2019

### 1.1.0 (October 10)

#### Added
* Moved the `/data` and `/meta` endpoints under a nation path to distinguish national automation index data.
* Added a new UK Automation Index dataset.

#### Deprecated
* `/meta` and `/data` root level endpoints are now deprecated in favor of `/us/meta` and `/us/data` respectively.

## September 2019

### 1.0.3 (September 25)

#### Refactored
* Refactored method of routing API endpoints.

## March 2019

### 1.0.2 (March 20)

#### Refactored
* Upgraded proxy web server.

### 1.0.1 (March 4)

#### Refactored
* Minor performance improvements.

## January 2019

### 1.0.0 (January 14)

#### Added
* Start a changelog to document updates to the API.
