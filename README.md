# Occupation Automation Index Service

#### Development Dependencies:
* Docker (running the app)

#### Run the app locally:
1. Clone the repo
2. Run `docker-compose up api`

#### Run tests:
`docker-compose run tests`

#### Verify data:
`docker-compose run api python ../scripts/schema-validator.py data/seed.schema.json data/seed.json`

#### Clean up containers:
`docker-compose down`

#### Rebuild images:
`docker-compose build`

#### Load test:
`ab -c 50 -n 500 -A username:password https://emsiservices.com/automation-index/data`

#### Update Changelog and Version
When updating the service make sure to update the changelog following the [Changelog Standard](https://economicmodeling.atlassian.net/wiki/spaces/MICRO/pages/468648123/Changelog+Standardization).

_Note these two files need to be updated when changing versions._

* `/docs/api.raml`
* `/app/config/prod.py`

#### Rebuild markdown documentation:
`scripts/generate-docs.sh` (run from project root)

Note that you'll need to have access to the [raml-to-md](https://gitlab.economicmodeling.com/emsi-services/raml-to-md) repository and have ssh keys set up properly for this script to work. If you really prefer cloning via https for some reason you can swap out the ssh git url in the script for the https one.

### Response format
API response format follows the Lightcast
[API Standards](https://economicmodeling.atlassian.net/wiki/display/ENGLEAD/API+Standards).
