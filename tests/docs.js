const test = require('ava');
const suite = require('raml-test-suite');

suite.examples.modify('GET /{nation} -> 404', request => {
    request.uriParameters.nation = 'not-a-naiton';
    return request;
});

suite.examples.modify('GET /{nation}/meta -> 404', request => {
    request.uriParameters.nation = 'not-a-naiton';
    return request;
});

suite.examples.modify('POST /{nation}/data -> 400', request => {
    request.body = {};
    return request;
});

suite.examples.modify('GET /{nation}/data -> 404', request => {
    request.uriParameters.nation = 'not-a-naiton';
    return request;
});

suite.examples.modify('POST /{nation}/data -> 404', request => {
    request.uriParameters.nation = 'not-a-naiton';
    return request;
});

suite.examples.register(test);
