const test = require('ava');
const suite = require('raml-test-suite');

let nationEndpoints = suite.request('GET', '/').data.endpoints.filter(e => e != '/status');
let nationCodes = {};

for(let nationEndpoint of nationEndpoints){
    // trim off leading '/' character
    const nation = nationEndpoint.slice(1);

    suite.test(
        `${nation}`,
        'GET /{nation} -> 200',
        request => {
            request.uriParameters.nation = nation;
            return request;
        }
    );

    suite.test(
        `${nation} meta`,
        'GET /{nation}/meta -> 200',
        request => {
            request.uriParameters.nation = nation;
            return request;
        }
    );

    suite.serial(
        `${nation} data`,
        'GET /{nation}/data -> 200',
        request => {
            request.uriParameters.nation = nation;
            return request;
        },
        (response, t) => {
            const codes = Object.keys(response.body.data)
            t.true(codes.length > 0);
            nationCodes[nation] = codes.slice(0, 10);
        }
    );

    suite.test(
        `${nation} data`,
        'POST /{nation}/data -> 200',
        request => {
            request.uriParameters.nation = nation;
            request.body = nationCodes[nation];
            return request;
        },
        (response, t) => {
            const codes = Object.keys(response.body.data);
            t.is(codes.length, nationCodes[nation].length);
            t.deepEqual(codes.sort(), nationCodes[nation].sort());
        }
    );
}

suite.register(test);
