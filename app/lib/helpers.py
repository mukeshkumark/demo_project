import json
import logging

from os import path, environ
from jsonschema import Draft7Validator
from functools import wraps, lru_cache

from app.config.prod import VERSION

from app.lib.util import ServerlessLogConf, ServerlessRequestContext

sls_log_conf = ServerlessLogConf(dsn=environ.get('SENTRY_DSN'))

logger = logging.getLogger('app')

class ApiException(Exception):
    """Base API exception, never use this directly"""
    pass

class InternalErrorException(ApiException):
    """Use when raising a 500 response"""
    def __init__(self, detail):
        self.error = InternalErrorResponse(detail)

class NotFoundException(ApiException):
    """Use when raising a 404 response"""
    def __init__(self, detail):
        self.error = NotFoundResponse(detail)

class BadRequestException(ApiException):
    """Use when raising a 400 response"""
    def __init__(self, detail):
        self.error = BadRequestResponse(detail)

def BaseResponse(data, status=200, headers={}):
    return {
        'statusCode': status,
        'body': data,
        'headers': {
            'X-Api-Version': VERSION,
            **headers
        }
    }

def JsonResponse(data, status=200, headers={}, key='data'):
    return BaseResponse(
        json.dumps({key: data}),
        status,
        headers={
            'Content-Type': 'application/json',
            **headers
        }
    )

def ErrorResponse(status, title, details=None, headers={}):
    if type(details) is not list:
        details = [details]

    data = [
        {
            'status': status,
            'title': title,
            'detail': detail
        }
        for detail in details
    ]
    return JsonResponse(data, status, headers, key='errors')

def InternalErrorResponse(detail=None, headers={}):
    return ErrorResponse(500, 'Internal server error', detail, headers)

def NotFoundResponse(detail=None, headers={}):
    return ErrorResponse(404, 'URL not found', detail, headers)

def BadRequestResponse(detail=None, headers={}):
    return ErrorResponse(400, 'Invalid request', detail, headers)

def content_type_application_json(headers):
    # HTTP spec allows for multiple headers of the same name, check all of them
    for k, v in headers.items():
        if (k.lower(), v) == ('content-type', 'application/json'):
            return True
    return False

def serverless_endpoint(*methods):
    """
    Decorate a function as a serverless endpoint, wrapping up common
    functionality for logging and error handling.
    """
    def decorator(fn):
        available_methods = frozenset(methods)
        @wraps(fn)
        def _serverless_endpoint_wrap(event, context, *args, **kwargs):
            with ServerlessRequestContext(event, sls_log_conf):
                try:
                    if available_methods and event['httpMethod'] not in available_methods:
                        return ErrorResponse(405, 'Method not allowed')

                    return fn(event, context, *args, **kwargs)
                except ApiException as e:
                    return e.error
                except Exception as e:
                    logger.exception(e, exc_info=True)
                    return InternalErrorResponse()
        return _serverless_endpoint_wrap
    return decorator

def validate(schema):
    """Validate the request json with given schema"""
    validator = Draft7Validator(schema)

    def decorator(fn):
        @wraps(fn)
        def _validate_wrap(event, context, *args, **kwargs):
            if not content_type_application_json(event['headers']):
                return ErrorResponse(
                    status=415,
                    title='Unsupported Content Type',
                    details='Request Content-Type must be \'application/json\''
                )

            try:
                body = json.loads(event['body'])
            except:
                return BadRequestResponse('Invalid request body')

            if not validator.is_valid(body):
                return BadRequestResponse([
                    error.message+(' ('+'.'.join(str(x) for x in error.path)+')' if error.path else '')
                    for error in sorted(validator.iter_errors(body), key=str)
                ])
            return fn(event, context, *args, **kwargs)
        return _validate_wrap
    return decorator

def load_json(file_name):
    with open(file_name) as fp:
        return json.load(fp)

@lru_cache(maxsize=128)
def load_file(file_name):
    with open(file_name) as fp:
        return fp.read()

def validate_nation(nation, nations):
    if nation not in nations:
        raise NotFoundException(f"""Invalid nation '{nation}', expecting one of: '{"','".join(sorted(nations))}'""")
