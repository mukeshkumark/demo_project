import logging

from raven.contrib.awslambda import LambdaClient
from raven.handlers.logging import SentryHandler
from raven.conf import setup_logging

class ServerlessLogConf:
    """Class that wraps up sentry logging configuration"""
    def __init__(self, dsn=None):
        self.client = LambdaClient(dsn=dsn)
        handler = SentryHandler(self.client, level=logging.WARN)
        setup_logging(handler)

    def configure_context(self, event):
        self.client.tags_context({
            'correlationId': event['headers'].get('X-Correlation-Id')
        })
        self.client.user_context({
            'id': event['headers'].get('X-Consumer-Id'),
            'username': event['headers'].get('X-Consumer-Username'),
            # grab the first IP from list of forwarded IPs
            'ip_address': event['headers'].get('X-Forwarded-For', '').rsplit(',')[0],
            'groups': event['headers'].get('X-Consumer-Groups'),
            'user-agent': event['headers'].get('User-Agent')
        })

class ServerlessRequestContext:
    """Context for use in 'with' clause for managing request context logging"""
    def __init__(self, event, sls_log_conf):
        self.sls_log_conf = sls_log_conf
        self.event = event

    def __enter__(self):
        self.sls_log_conf.client.context.activate()
        self.sls_log_conf.configure_context(self.event)
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        self.sls_log_conf.client.context.clear()
        self.sls_log_conf.client.context.deactivate()
