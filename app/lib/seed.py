from app.lib.helpers import load_json
from jsonschema import validate

# load up the seed data
seed_data = {
    'us': load_json('app/data/us_seed.json'),
    'uk': load_json('app/data/uk_seed.json')
}

# make sure it's valid
schema = load_json('app/data/seed.schema.json')

for data in seed_data.values():
    validate(data, schema)
