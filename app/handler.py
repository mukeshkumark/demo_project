import json
import logging

from app.lib.helpers import (
    JsonResponse, NotFoundResponse, BaseResponse, serverless_endpoint,
    validate, validate_nation, load_json, load_file
)
from app.lib.seed import seed_data

NATIONS = frozenset(seed_data.keys())

logger = logging.getLogger(__name__)

@serverless_endpoint()
def not_found(event, context):
    return NotFoundResponse()

@serverless_endpoint('GET')
def status(event, context):
    return JsonResponse({
        'message': 'Service is healthy',
        'healthy': True
    })

@serverless_endpoint('GET')
def root(event, context):
    return JsonResponse({
        'endpoints': [
            f'/{e}' for e in sorted(['status', *NATIONS])
        ]
    })

@serverless_endpoint('GET')
def nation(event, context):
    nation = event['pathParameters']['nation']
    validate_nation(nation, NATIONS)
    return JsonResponse({'endpoints': [f'/{nation}/meta', f'/{nation}/data']})

@serverless_endpoint('GET')
def meta(event, context):
    nation = event['pathParameters']['nation']
    validate_nation(nation, NATIONS)
    return JsonResponse({
        'taxonomies': {
            'soc': seed_data[nation]['soc-hierarchy-id']
        },
        'attribution': {
            'title': 'Automation Index Data',
            'body': seed_data[nation]['attribution']
        }
    })

@serverless_endpoint('GET')
def get_data(event, context):
    nation = event['pathParameters']['nation']
    validate_nation(nation, NATIONS)
    return JsonResponse(seed_data[nation]['data'])

@serverless_endpoint('POST')
@validate(load_json('docs/schemas/requests/data.schema.json'))
def post_data(event, context):
    nation = event['pathParameters']['nation']
    validate_nation(nation, NATIONS)
    return JsonResponse({
        str(county): seed_data[nation]['data'].get(str(county))
        for county in json.loads(event['body'])
    })

@serverless_endpoint('GET')
def docs(event, context):
    return BaseResponse(load_file('docs/api.md'), headers={ 'Content-Type': 'text/plain' })

@serverless_endpoint('GET')
def changelog(event, context):
    return BaseResponse(load_file('docs/changelog.md'), headers={ 'Content-Type': 'text/plain' })
