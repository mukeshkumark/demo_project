#!/usr/bin/env python3

from jsonschema import validate
import json
import sys

if len(sys.argv) < 3:
    print("Usage: "+sys.argv[0]+" <json-schema-file> <json-file>")
    sys.exit(1)

schema_file = open(sys.argv[1])
json_file = open(sys.argv[2])

try:
    validate(json.load(json_file), json.load(schema_file))
except Exception as e:
    print(e.message)
    sys.exit(1)

print("Valid json")
