#!/bin/bash

echo -e "\n\nGenerating markdown..."

docker run --rm \
    -v $(pwd)/docs:/docs \
    -e "URL=https://emsiservices.com/automation-index" \
    emsivegas/raml-to-md
