#!/usr/bin/env bash

set -e

URL=$1

CLIENT=${CI_OAUTH_USER?CI_OAUTH_USER env variable required}
SECRET=${CI_OAUTH_PASSWORD?CI_OAUTH_PASSWORD env variable required}
SCOPE=${SCOPE?SCOPE env variable required}

if [ -z "$URL" ]; then
    echo "Usage: $(basename $0) <URL>"
    echo "  i.e. $(basename $0) http://url.app.com"
    exit 1
fi

echo "Testing $URL"

docker-compose pull tests
docker-compose run \
    --no-deps \
    --rm \
    -e URL=$URL \
    -e EMSI_CLIENT_ID=$CLIENT \
    -e EMSI_CLIENT_SECRET=$SECRET \
    -e EMSI_SCOPE=$SCOPE \
    tests || ( docker-compose down --remove-orphans --volumes --rmi all; exit 1 )

docker-compose down --remove-orphans --volumes --rmi all
