#!/usr/bin/env bash

set -e

DESTINATION=$1

case $DESTINATION in
    prod) ;;
    qa) ;;
    *)
        echo "Invalid deploy destination, expected 'prod' or 'qa'"
        exit 1
        ;;
esac

docker-compose build
docker-compose run --rm --no-deps \
    -e AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID?AWS_ACCESS_KEY_ID env variable required} \
    -e AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY?AWS_SECRET_ACCESS_KEY env variable required} \
    # -e SENTRY_DSN=${SENTRY_DSN?SENTRY_DSN env variable required} \
    api \
    serverless deploy --force -s $DESTINATION -r $AWS_REGION
