module.exports.ipWhitelist = () => [
    // west-prod AWS NAT ip
    '52.38.228.234',
    // east-prod AWS NAT ip
    '18.210.132.41'
];

// east-prod private subnets
module.exports.subnetIds = () => [
    'subnet-06721538e437dbed9',
    'subnet-0772f6215cb2f1fa1',
    'subnet-095aeef9477213b53',
    'subnet-0a260b844a920bfd3',
    'subnet-0dadf2ca70610c339'
];

// east-prod lambda security group
module.exports.securityGroupIds = () => [
    'sg-0e860807827a11ea6'
];
