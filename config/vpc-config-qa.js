module.exports.ipWhitelist = () => [
    // // west-qa AWS NAT ip
    // '52.35.14.213',
    // // east-qa AWS NAT ip
    // '18.207.49.52'
    '0.0.0.0/0'
];

// east-qa private subnets
module.exports.subnetIds = () => [
    'subnet-0500d7ac15afc72a2',
    'subnet-01c6267751be1b867'
    
];

// east-qa lambda security group
module.exports.securityGroupIds = () => [
    'sg-0c373fc964699242a'
];